<?php

use judahnator\GdprShield\Geolocation\GeoPluginDriver;

return [

    // The message to be displayed on the GDPR abort page
    'abort_message' => 'Due to GDPR compliance issues, we will not serve EU customers',

    /*
     * The driver to use for geolocation lookups.
     *
     * The default GeoPluginDriver will query the http://www.geoplugin.net site to look up IP addresses.
     * You also have the option to use the GeoIP2GeolocationDriver, which provides better speed at the cost
     * of a larger memory footprint.
     *
     * You may also use your own driver. Just make sure to implement the GeoLocationDriverInterface interface.
     */
    'geolocation_driver' => GeoPluginDriver::class,

    /*
     * This option is only required if you use the GeoIP2GeolocationDriver.
     * This field needs to point to the full path of your geolite2 database.
     *
     * You can find one here: https://dev.maxmind.com/geoip/geoip2/geolite2/
     * Be sure to pay attention to the licence section on that page, if you use this database please give them due credit.
     */
    'geolite_database_path' => null

];