@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-6">
                <div class="well">
                    {{ (!empty($File->notes) ? $File->notes : "No notes provided") }}
                </div>
            </div>

            <div class="col-md-6">
                <a href="{{ $File->download_url }}" class="btn btn-default btn-lg btn-block">Download File</a>
            </div>

        </div>

        <div class="row">

            <div class="col-md-6">
                <table class="table table-striped table-hover ">
                    <tbody>
                    <tr>
                        <th>Name</th>
                        <td>{{ $File->clientName }}</td>
                    </tr>
                    <tr>
                        <th>Size</th>
                        <td>{{ $File->size->pretty }}</td>
                    </tr>
                    <tr>
                        <th>Downloads</th>
                        <td>{{ $File->downloads }}</td>
                    </tr>
                    <tr>
                        <th>Bandwidth</th>
                        <td>{{ $File->bandwidth->pretty }}</td>
                    </tr>
                    <tr>
                        <th>Upload Date</th>
                        <td>{{ $File->created_at->format('m-d-Y') }}</td>
                    </tr>
                    <tr>
                        <th>Expires</th>
                        <td>{{ (!is_null($File->expires_at) ? $File->expires_at->format('m-d-Y') : "Never") }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            @if(!\Auth::guest() && !is_null($File->user) && $File->user->id == \Auth::user()->id)
                <div class="col-md-6">
                    <div class="panel-group">

                        <div class="panel panel-default" style="margin-bottom: 20px;">

                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#collapse1">Administration Options</a>
                                </h4>
                            </div>

                            <div id="collapse1" class="panel-collapse collapse">

                                <ul class="list-group">

                                    <li class="list-group-item">
                                        <input onchange="updateFile()" id="expires_at" type="number" step="1" min="0" class="form-control" placeholder="Delay Expiration By Days">
                                    </li>

                                    <li class="list-group-item">
                                        <select onchange="updateFile()" id="visibility" class="form-control">
                                            <option disabled selected>Change Visibility To...</option>
                                            <option value="public">Public</option>
                                            <option value="private">Private</option>
                                            <option value="unlisted">Unlisted</option>
                                        </select>
                                    </li>

                                    @push('scripts')
                                    <script>
                                        function updateFile() {

                                            var payload = {
                                                '_method': 'PATCH',
                                                '_token': '{{ csrf_token() }}'
                                            };

                                            // Set the expires_at variable
                                            var expires_at = $('#expires_at').val();

                                            // If it is not an empty string (placeholder edited)
                                            if (expires_at !== "") {
                                                // Set the expires_at variable
                                                payload['expires_at'] = expires_at;
                                            }

                                            // Set the visibility variable
                                            var visibility = $('#visibility').val();

                                            // If the visibility is not null
                                            if (visibility !== null) {
                                                // Set the visibility variable
                                                payload['visibility'] = visibility;
                                            }

                                            $.post('{{ url('files',[$File->id]) }}',payload)
                                                .done(function() {
                                                    $('#successMessage').text("File updated successfully");
                                                    $('#success').show('slow');
                                                    setTimeout(function() {
                                                        $('#success').hide('slow');
                                                        window.location.href = '{{ url("files",[$File->id]) }}';
                                                    },2000)
                                                })
                                                .fail(function() {
                                                    $('#errorMessage').text("Updating file failed");
                                                    $('#error').show('slow');
                                                    setTimeout(function () {
                                                        $('#error').hide('slow');
                                                    },2000);
                                                });
                                        }
                                    </script>
                                    @endpush

                                    <li class="list-group-item">

                                        <button id="delete" class="btn btn-block btn-danger">Delete File</button>

                                        @push('scripts')
                                        <script>
                                            $('#delete').on('click',function () {
                                                $.post('{{ url('files',[$File->id]) }}',{
                                                    '_token': '{{ csrf_token() }}',
                                                    '_method': 'delete'
                                                }).fail(function() {
                                                    $('#errorMessage').text("Deletion of this file failed");
                                                    $('#error').show('slow');
                                                    setTimeout(function () {
                                                        $('#error').hide('slow');
                                                    },2000);
                                                }).done(function() {
                                                    $('#successMessage').text("File deleted successfully");
                                                    $('#success').show('slow');
                                                    setTimeout(function() {
                                                        $('#success').hide('slow');
                                                        window.location.href = '{{ url("/") }}';
                                                    },2000)
                                                })
                                            })
                                        </script>
                                        @endpush

                                    </li>

                                </ul>

                            </div>

                        </div>

                        <div id="error" style="display:none" class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>Error!</strong> <p id="errorMessage"></p>
                        </div>

                        <div id="success" style="display: none" class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>Success!</strong> <p id="successMessage"></p>
                        </div>

                    </div>
                </div>
            @endif

        </div>
    </div>
@endsection