<html lang="en">
<head>

    <meta charset="utf-8">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{ url('css/bootstrap.min.css') }}" media="screen">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

<body>

<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a href="../" class="navbar-brand">{{ config('app.name', 'Laravel') }}</a>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">

            <ul class="nav navbar-nav navbar-right">
            @if(\Auth::guest())
                <li><a href="{{ url('login') }}">Login</a></li>
                <li><a href="{{ url('register') }}">Register</a></li>
            @else
                <li class="dropdown">

                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                        {{ \Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu">
                        <li><a href="{{ url('home') }}">Home</a></li>
                        <li><a href="{{ route('tokens.index') }}">Access Tokens</a></li>
                        <li>
                            <a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                        </li>
                    </ul>

                    @push('scripts')
                        <form id="logout-form"
                              action="{{ url('/logout') }}"
                              method="POST"
                              style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    @endpush

                </li>
            @endif
            </ul>

        </div>
    </div>
</div>

<div style="padding-top:70px">

    <div class="container">
        @if($errors->any())
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="alert alert-dismissible alert-danger">

                        <button type="button" class="close" data-dismiss="alert">×</button>

                        <strong>Error</strong>

                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>

                    </div>
                </div>
            </div>
        @endif
    </div>

    @yield('content')

</div>

<footer>
    <div class="row">
        <div class="col-lg-12">
            @yield('footer')
        </div>
    </div>
</footer>


<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="{{ url('js/bootstrap.min.js') }}"></script>
@stack('scripts')
