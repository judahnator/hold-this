@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="page-header">
            <h1>Forbidden</h1>
            <h2>{{ $exception->getMessage() }}</h2>
        </div>
    </div>
@endsection