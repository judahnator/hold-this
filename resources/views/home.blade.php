@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row">

        <div class="col-md-4">

            <div class="page-header" data-toggle="tooltip" data-placement="top" title="Does not include expired items">
                <h1>My Stats</h1>
            </div>

            <table class="table table-striped table-hover ">
                <tbody>
                <tr>
                    <th>Total Files</th>
                    <td>{{ \Auth::user()->files->count() }}</td>
                </tr>
                <tr>
                    <th>Disk Consumed</th>
                    <td>{{ \Auth::user()->disk_usage->pretty }}</td>
                </tr>
                <tr>
                    <th>Bandwidth Used</th>
                    <td> {{ \Auth::user()->bandwidth_usage->pretty }}</td>
                </tr>
                </tbody>
            </table>

        </div>

        <div class="col-md-8">

            <div class="page-header">
                <h1>My Files</h1>
            </div>

            @if(\Auth::user()->files->isEmpty())
                <div class="alert alert-info">
                    There are no files currently associated with your account
                </div>
            @else
                <table class="table table-striped table-hover ">

                    <thead>
                    <tr>
                        <th>File Name</th>
                        <th>Notes</th>
                        <th>Visibility</th>
                        <th>Downloads</th>
                        <th>Expires At</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach(\Auth::user()->files->sortBy('clientName') as $file)
                        <tr>
                            <td><a href="{{ url('files/'.$file->id) }}">{{ $file->clientName }}</a></td>
                            <td>{{ strlen($file->notes) > 20 ? substr($file->notes,0,20).'...' : $file->notes }}</td>
                            <td>{{ $file->visibility }}</td>
                            <td>{{ $file->downloads }}</td>
                            <td>{{ $file->expires_at->format('m-d-Y') }}</td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
            @endif

        </div>

    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="page-header">
                <h2>Monthly Disk Utilization</h2>
            </div>

            <div title="This takes into account expired files which have been removed less than a month ago" data-toggle="tooltip" data-placement="top" class="progress" style="margin-bottom:0;">
                <div class="progress-bar progress-bar-info" style="width: {{ \Auth::user()->monthly_utilization->raw / (10**7) }}%"></div>
            </div>

        </div>
    </div>

</div>
@endsection

@push('scripts')
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
@endpush