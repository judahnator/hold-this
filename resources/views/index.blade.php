@extends('layouts.app')

@section('content')
    <div class="container">

        <form action="{{ url('files') }}" method="post" enctype="multipart/form-data">

            {!! csrf_field() !!}

            <div class="row">

                <div class="col-md-4">

                    <div class="page-header">
                        <h2>Notes</h2>
                    </div>

                    <textarea name="notes" style="min-height: 60px;" class="form-control" placeholder="Check out my awesome file!{{ PHP_EOL }}(optional)"></textarea>

                </div>

                <div class="col-md-8">

                    <div class="page-header">
                        <h2>File</h2>
                    </div>

                    <input style="min-height: 60px;padding-top:18px;" type="file" class="form-control" placeholder="File" id="file" name="file">

                </div>

            </div>

            <div class="row">

                <div class="col-md-6">

                    <div class="page-header">
                        <h2>Options:</h2>
                    </div>

                    <div class="row">

                        <div class="col-md-6">

                            <h4>Expires After</h4>

                            <select name="expires_at" class="form-control">
                                <option value="1">1 Day</option>
                                <option value="7">1 Week</option>
                                <option value="30">1 Month</option>
                                <option value="365">1 Year</option>
                                <option value="0" {{ (\Auth::guest() ? "disabled" : "") }}>Never{{ (\Auth::guest() ? " (Members Only!)" : "") }}</option>
                            </select>

                        </div>

                        <div class="col-md-6">

                            <h4>Exposure</h4>

                            <select class="form-control" name="visibility">
                                <option value="public" selected>Public</option>
                                <option value="unlisted">Unlisted</option>
                                <option @if(\Auth::guest()) disabled @endif value="private">Private</option>
                            </select>

                        </div>

                    </div>

                </div>

                <div class="col-md-6">

                    <div class="page-header">
                        <h2>Pricing:</h2>
                    </div>

                    <p><b>Free</b> for beta testers</p>

                    <br>

                    <button type="button" id="submit" class="btn btn-primary pull-right">Make It So!</button>

                    @push('scripts')
                        <script>
                            $('#submit').on('click',function() {
                                $('#progressDiv').show();
                                $('#submit').attr('disabled',true);
                                $.ajax({
                                    url: '{{ url('files/upload') }}',
                                    type: 'POST',
                                    data: new FormData($('form')[0]),
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    xhr: function() {
                                        var myXhr = $.ajaxSettings.xhr();
                                        if (myXhr.upload) {
                                            // For handling the progress of the upload
                                            myXhr.upload.addEventListener('progress', function(e) {
                                                if (e.lengthComputable) {
                                                    $('#progress').css('width',((e.loaded / e.total) * 100) + '%');
                                                }
                                            } , false);
                                        }
                                        return myXhr;
                                    }
                                }).done(function(response) {
                                        window.location.href = "{{ url('files') }}/" + response['id'];
                                    });
                            })
                        </script>
                    @endpush
                </div>

            </div>

            <div class="row" style="padding-top:40px">
                <div class="col-md-6 col-md-offset-3">
                    <div id="progressDiv" class="progress progress-striped active" style="display: none">
                        <div id="progress" class="progress-bar" style="width: 0"></div>
                    </div>
                </div>
            </div>

        </form>

    </div>
@endsection