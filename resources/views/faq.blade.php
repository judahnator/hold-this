@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="page-header">
            <h1>Site FAQ</h1>
        </div>

        <div class="row">

            <div class="col-md-4">
                <div class="well">
                    You can find most information about this site on the page. If I missed anything, feel free to use
                    the contact form I have not made yet to contact me.<br>
                    I know documentation is a bit light, ill get around to better documentation eventually.
                </div>
            </div>

            <div class="col-md-8">

                <div class="panel-group">

                    <div class="panel panel-default">

                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#collapse1">Why does this site exist</a>
                            </h4>
                        </div>

                        <div id="collapse1" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>
                                    I made this site with a friend in mind. He needed to make frequent off-site backups
                                    of one of his servers, but had an incredibly slow internet connection and aggressive
                                    bandwidth caps. He had a bash script to upload files to DropBox, but it was slow
                                    and cumbersome.<br>
                                    We were talking about the problem one day and he mentioned that he wished there was
                                    a site out there that he could just tell to "Hold This" file for a little bit. And
                                    thus the site was born.<br><br>
                                    The site is not meant to be too complicated. It has a simple web interface, an easy
                                    to use API, and that's it.
                                </p>
                            </div>
                        </div>

                    </div>

                    <div class="panel panel-default">

                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#collapse2">Is it free?</a>
                            </h4>
                        </div>

                        <div id="collapse2" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>
                                    Short answer: Yes.<br>
                                    Long answer: Kinda Yes.<br>
                                </p>
                                <p>
                                    Short term I will allow users to store a maximum of 1GB per month, and I am
                                    calculating usage in timed byte-hours. Without going into too much detail, that
                                    basically means you can store a 1GB file for a month or a 30GB file for a day.
                                </p>
                                <p>
                                    Long term I will implement a paid option. I don't plan on turning a profit, but for
                                    users who want to use this commercially or whose storage requirements are greater
                                    than can be reasonably expected from a free service, I will charge at a not yet
                                    determined rate.
                                </p>
                            </div>
                        </div>

                    </div>

                    <div class="panel panel-default">

                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#collapse3">Are my files safe?</a>
                            </h4>
                        </div>

                        <div id="collapse3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>
                                    No.<br>
                                    There are safeguards in place to prevent unauthorized users from accessing your
                                    data, but you should never upload any sensitive data to a third party (in this case
                                    that is me).
                                </p>
                            </div>
                        </div>

                    </div>

                    <div class="panel panel-default">

                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#collapse4">How do I use the API?</a>
                            </h4>
                        </div>

                        <div id="collapse4" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>
                                    In order to use the API you will need an <i>'API Access Token'</i>. You can manage
                                    your access tokens <a href="{{ route('tokens.index') }}">here</a>.
                                </p>
                                <p>
                                    The basic usage is fairly straightforward. The token goes in the Authorization
                                    header, but the rest is up to the implementation.<br>
                                    The available routes are as follows:
                                </p>
                                <div class="list-group">

                                    <a href="#" class="list-group-item">
                                        <h4 class="list-group-item-heading">/api/user</h4>
                                        <p>
                                            <b>GET:</b> Returns the currently logged in users info
                                        </p>
                                    </a>

                                    <a href="#" class="list-group-item">
                                        <h4 class="list-group-item-heading">/api/files</h4>
                                        <p>
                                            <b>GET:</b> Returns a list of all your files<br>
                                            <b>POST:</b> Uploads a few file
                                        </p>
                                    </a>

                                    <a href="#" class="list-group-item">
                                        <h4 class="list-group-item-heading">/api/files/{id}</h4>
                                        <p>
                                            <b>GET:</b> Returns information on a specific file<br>
                                        </p>
                                    </a>

                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>


    </div>
@endsection