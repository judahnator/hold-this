@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="page-header">
            <h1>API Access Tokens</h1>
        </div>

        <div class="row">

            <div class="col-md-4">
                <div class="well">
                    On this page you can create and manage access tokens for third party applications.
                </div>
            </div>

            <div class="col-md-8">
                <table class="table table-striped">

                    <thead>
                    <tr>
                        <th style="width:40%">Key</th>
                        <th style="width:40%">Created At</th>
                        <th style="width:20%"></th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach(\Auth::user()->tokens as $token)
                        <tr id="TokenTr_{{ $token->id }}">
                            <td>{{ $token->key }}</td>
                            <td>{{ $token->created_at->format('m-d-Y') }}</td>
                            <td>
                                <button onclick="deleteToken({{ $token->id }})" class="btn btn-danger">Delete</button>
                            </td>
                        </tr>
                    @endforeach
                    <tr id="NewToken">
                        <td colspan="3">
                            <button id="NewTokenButton" class="btn btn-block btn-success">Generate New Token</button>
                        </td>
                    </tr>
                    </tbody>

                </table>
            </div>

        </div>

    </div>
@endsection

@push('scripts')
<script>

    $('#NewTokenButton').on('click',function() {
        $.post('{{ route('tokens.store') }}',{
            '_token': '{{ csrf_token() }}'
        })
            .done(function(Response) {
                $('#NewToken').before(
                    '<tr id="TokenTr_' + Response['id'] + '">' +
                        '<td>' + Response['key'] + '</td>' +
                        '<td>Just Now</td>' +
                        '<td>' +
                            '<button onclick="deleteToken(' + Response['id'] + ')" class="btn btn-danger">Delete</button>' +
                        '</td>' +
                    '</tr>'
                );
            });
    });

    function deleteToken(TokenID) {
        $.post('{{ route('tokens.destroy',null) }}/' + TokenID,{
            '_method': 'DELETE',
            '_token': '{{ csrf_token() }}'
        })
            .done(function() {
                $('#TokenTr_' + TokenID).remove();
            });
    }

</script>
@endpush