<?php

namespace App\Traits;


use App\File;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;

trait FileControllerTrait
{

    public function index() {
        return \Auth::user()->files;
    }

    public function show($id) {
        $File = File::with('user')->findOrFail($id);
        if (
            $File->visibility === "private" &&
            (
                \Auth::guest() ||
                \Auth::user()->id != $File->user->id
            )
        ) {
            throw new UnauthorizedException('You are not authorized to view this file');
        }
        return $File;
    }

    public function store(Request $request) {

        $this->validate($request,[
            'file' => 'required|file|max:10000000', // 10GB
            'expires_at' => 'integer',
            'notes' => 'nullable|string',
            'visibility' => 'in:public,private,unlisted'
        ]);

        $File = new File();
        $File->notes = (is_null($request->get('notes')) ? '' : $request->get('notes'));
        $File->visibility = $request->get('visibility','private');
        $File->clientName = $request->file('file')->getClientOriginalName();
        $File->internalName = $request->file('file')->getFilename();

        if ($request->get('expires_at',1) > 0) {
            $File->expires_at = Carbon::now()->addDays($request->get('expires_at',1));
        }

        $request->file('file')->move(storage_path('app/files/'));

        if (\Auth::user()) {
            $File->user()->associate(\Auth::user());
        }

        $File->save();

        return $File;

    }

}