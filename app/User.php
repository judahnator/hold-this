<?php

namespace App;

use \Helpers;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes to append to the model
     *
     * @var array
     */
    protected $appends = [
        'bandwidth_usage',
        'disk_usage',
        'monthly_utilization'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function files() {
        return $this->hasMany(File::class);
    }

    public function getDiskUsageAttribute() {
        return \Bytes::sum($this->files->pluck('size')->toArray());
    }

    public function getBandwidthUsageAttribute() {
        $Bytes = \Bytes::make();
        foreach ($this->files as $file) {
            $Bytes->add($file->size->raw * $file->downloads);
        }
        return $Bytes;
    }

    /**
     * Returns the monthly usage accounting for expired files
     * @return \Illuminate\Support\Collection
     */
    public function getMonthlyUtilizationAttribute() {
        return Helpers::calculateTimedByteHoursForUser($this);
    }

    /**
     * The relationship with the users tokens
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tokens() {
        return $this->hasMany(Token::class);
    }

}
