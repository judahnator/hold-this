<?php

namespace App\Helpers;


use App\User;
use Carbon\Carbon;

class Helpers
{

    /**
     * @param integer|User $User
     * @return \Illuminate\Support\Collection
     */
    public static function calculateTimedByteHoursForUser($User) {

        // If being passed an integer, find the user
        if (is_int($User)) {
            $User = User::findOrFail($User);
        }

        // Get all files from a user, include deleted items
        $Files = $User->files()->withTrashed()->get();

        // The total byte-hours
        $ByteHours = 0;

        // For each of the users files
        foreach ($Files as $File) {

            if ($File->created_at->gt(Carbon::now()->subMonth())) {

                // If the file was created more than a month ago, only count this months data
                $StartTime = Carbon::now()->subMonth();

            }else {

                // If the file was created less than a month ago, use its 'created_at' field
                $StartTime = $File->created_at;

            }

            if ($File->trashed()) {

                // If the file has been deleted end at its deletion time
                $EndTime = $File->deleted_at;

            }else {

                // If the file has not been deleted at, calculate to the current time
                $EndTime = Carbon::now();

            }

            // Multiply the files size by the number of hours it lived for
            $ByteHours += ($StartTime->diffInHours($EndTime)) * $File->size->raw;

        }

        // Divide the size by the number of hours in a month
        $ByteMonths = (int)floor($ByteHours / 720);

        // Return the standard size collection
        return \Bytes::make($ByteMonths);

    }

}