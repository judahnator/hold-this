<?php

namespace App;

use App\Events\FileCreated;
use App\Events\FileDeleted;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Schema;

class File extends Model
{

    use SoftDeletes;

    protected $appends = [
        'file_size',
        'download_url'
    ];

    protected $casts = [
        'clientName' => 'string',
        'internalName' => 'string',
        'notes' => 'string',
        'visibility' => 'string'
    ];

    protected $dates = [
        'created_at',
        'deleted_at',
        'expires_at'
    ];

    protected $events = [
        'created' => FileCreated::class,
        'deleted' => FileDeleted::class
    ];

    protected $fillable = [
        'clientName',
        'expires_at',
        'internalName',
        'notes',
        'user_id',
        'visibility',
    ];

    protected $table = 'files';

    public $timestamps = true;

    public function __construct(array $attributes = [])
    {
        if (!\Cache::has('file_migration_ran')) {
            \Cache::put(
                'file_migration_ran',
                Schema::hasColumn($this->getTable(), 'size'),
                1
            );
        }
        parent::__construct($attributes);
    }

    /**
     * Retrieves the overall bandwidth consumed by this specific file
     * @return \Bytes
     */
    public function getBandwidthAttribute() {
        $rawSize = $this->downloads * $this->size->raw;
        return \Bytes::make($rawSize);
    }

    public function getDownloadUrlAttribute() {
        return route('download',[$this->id,$this->clientName]);
    }

    /**
     * Returns a collection with the Bytes object elements.
     * This only exists because Laravel cant serialize the class right
     *
     * @return \Illuminate\Support\Collection
     */
    public function getFileSizeAttribute() {
        $Bytes = $this->size;
        return collect([
            'pretty' => $Bytes->pretty,
            'raw' => $Bytes->raw
        ]);

    }

    /**
     * Gets the size of the given file
     * @param integer $value
     * @return \Bytes
     */
    public function getSizeAttribute($value) {
        if (\Cache::get('file_migration_ran',false) && $value > 0) {
            $Size = $value;
        }elseif (\File::exists(storage_path('app/files/'.$this->internalName))) {
            $Size = \File::size(storage_path('app/files/'.$this->internalName));
        }else {
            $Size = 0;
        }
        return \Bytes::make($Size);
    }

    /**
     * If the migration to add the file size has ran, save the data
     * Otherwise do nothing
     * @param \Bytes $value
     */
    public function setSizeAttribute(\Bytes $value) {
        if (\Cache::get('file_migration_ran',false)) {
            $this->attributes['size'] = $value->raw;
        }
    }

    /**
     * The File->User relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

}
