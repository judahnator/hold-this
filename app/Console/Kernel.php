<?php

namespace App\Console;

use App\Console\Commands\ClearDeletedEntries;
use App\Console\Commands\ClearExpiredFiles;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ClearDeletedEntries::class,
        ClearExpiredFiles::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('files:clear-expired')
            ->hourly()
            ->thenPing('http://beats.envoyer.io/heartbeat/0kQo1lOksvvTCoK');
        $schedule->command('files:clear-deleted')
            ->daily()
            ->thenPing('http://beats.envoyer.io/heartbeat/AveEZdugyJeFI33');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
