<?php

namespace App\Console\Commands;

use App\File;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ClearExpiredFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'files:clear-expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command is to be used to permanently delete any files that are past-due for expiration';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {

        $Files = File::where('expires_at','<=',Carbon::now())->get();

        $this->output->writeln('There are '.$Files->count().' files to remove');

        $this->output->progressStart($Files->count());
        foreach ($Files as $File) {
            $File->delete();
            $this->output->progressAdvance();
        }
        $this->output->progressFinish();

        $this->output->success('File removal complete!');

    }
}
