<?php

namespace App\Console\Commands;

use App\File;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ClearDeletedEntries extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'files:clear-deleted';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will clear out the softdeleted items older than one month';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $Files = File::withTrashed()
            ->whereNotNull('deleted_at')
            ->where('deleted_at','<',Carbon::now()->subMonth())
            ->get();
        foreach ($Files as $File) {
            $File->forceDelete();
        }
        $this->output->writeln($Files->count().' entries removed from the database.');
    }
}
