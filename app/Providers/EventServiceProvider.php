<?php

namespace App\Providers;

use App\Events\FileCreated;
use App\Events\FileDeleted;
use App\Listeners\CalculateFileSize;
use App\Listeners\DeleteFileFromSystem;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        FileCreated::class => [
            CalculateFileSize::class,
        ],
        FileDeleted::class => [
            DeleteFileFromSystem::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
