<?php

namespace App\Listeners;

use App\Events\FileDeleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeleteFileFromSystem
{

    /**
     * Handle the event.
     *
     * @param  FileDeleted  $event
     * @return void
     */
    public function handle(FileDeleted $event)
    {
        $FilePath = storage_path('app/files/'.$event->file->internalName);
        if (\File::exists($FilePath)) {
            \File::delete($FilePath);
        }
    }

}
