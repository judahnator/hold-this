<?php

namespace App\Listeners;

use App\Events\FileCreated;
use Illuminate\Events\Dispatcher;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CalculateFileSize
{

    /**
     * Handle the event.
     *
     * @param  FileCreated  $event
     * @return void
     */
    public function handle(FileCreated $event)
    {
        $file = $event->file;
        $file->size = \Bytes::make(
            \File::size(storage_path('app/files/'.$event->file->internalName))
        );
        $file->save();
    }

}
