<?php

namespace App\Http\Middleware;

use App\Token;
use Closure;
use Illuminate\Auth\AuthenticationException;

class ApiAuthentication
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws AuthenticationException
     */
    public function handle($request, Closure $next)
    {

        // If no auth header sent, user is unauthorized
        if (!$request->hasHeader('Authorization'))
            throw new AuthenticationException();

        // Find the token
        $Token = Token::where('key',$request->header('Authorization'))->first();

        // No token found, user is not authorized
        if (is_null($Token))
            throw new AuthenticationException();

        // Log the user in
        \Auth::loginUsingId($Token->user_id);

        // Continue with the request
        return $next($request);

    }

}