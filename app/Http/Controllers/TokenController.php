<?php

namespace App\Http\Controllers;

use App\Token;
use Faker\Factory;
use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;

class TokenController extends Controller
{

    /**
     * TokenController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Deletes the token
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) {

        // Find the token or fail out
        $Token = Token::with('user')->findOrFail($id);

        // If the user does not own this token throw an unauthorized exception
        if ($Token->user->id != \Auth::user()->id) {
            throw new UnauthorizedException('You are not authorized to delete this users token');
        }

        // Remove the token
        $Token->delete();

        // Return success
        return response()->json(['success']);

    }

    /**
     * Show the users access tokens
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('tokens');
    }

    /**
     * Create a new access token
     * @return \Illuminate\Http\JsonResponse
     */
    public function store() {
        $key = '';
        for ($i=0;$i<12;$i++) {
            $key .= Factory::create()->randomLetter;
        }
        return response()->json(\Auth::user()->tokens()->create(['key' => $key]));
    }

}
