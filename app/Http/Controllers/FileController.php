<?php

namespace App\Http\Controllers;


use App\File;
use App\Traits\FileControllerTrait;
use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;

class FileController extends Controller
{

    use FileControllerTrait {
        show as TraitShow;
        store as TraitStore;
    }

    public function destroy($id) {
        $File = File::with('user')->findOrFail($id);
        if (\Auth::user()->id != $File->user->id) {
            throw new UnauthorizedException('You are not authorized to delete this file');
        }
        $File->delete();
        return response()->json([
            'action' => 'delete',
            'status'=>'ok'
        ],200);
    }

    public function store(Request $request) {
        return redirect(url('files/'.$this->TraitStore($request)->id));
    }

    public function show($id) {
        return view('show',['File'=>$this->TraitShow($id)]);
    }

    public function update($id, Request $request) {
        $File = File::with('user')->findOrFail($id);
        if ($File->user->id != \Auth::user()->id) {
            throw new UnauthorizedException("You are not authorized to update this file");
        }
        $this->validate($request,[
            'visibility' => 'in:public,private,unlisted',
            'expires_at' => 'integer|min:0'
        ]);
        if ($request->has('visibility')) {
            $File->visibility = $request->get('visibility');
        }
        if ($request->has('expires_at')) {
            $File->expires_at = $File->expires_at->addDays($request->get('expires_at'));
        }
        $File->save();
        return $File;
    }

    public function download($id,$filename) {

        $File = File::findOrFail($id);

        // If accessing by a different name than the file is known by,
        // redirect to the correct URL for SEO reasons
        if ($File->clientName != $filename) {
            return redirect(url('files/'.$id.'/'.$File->clientName));
        }

        if (
            $File->visibility == "private" &&
            (
                \Auth::guest() ||
                \Auth::user()->id != $File->user->id
            )
        ) {
            return redirect(url('/'))->withErrors(['message'=>'You are not authorized to view that file']);
        }

        $File->downloads++;

        $File->save();

        return response()->download(storage_path('app/files/'.$File->internalName),$File->clientName);

    }

    public function upload(Request $request) {
        return $this->TraitStore($request);
    }

}