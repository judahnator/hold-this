<?php

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApiController extends Controller
{

    protected function buildFailedValidationResponse(Request $request, array $errors)
    {
        return response()->json($errors,400);
    }

}