<?php

namespace App\Http\Controllers\API;


use App\File;
use App\Traits\FileControllerTrait;
use Illuminate\Http\Request;

class FileController extends ApiController
{

    use FileControllerTrait {
        index as TraitIndex;
        show as TraitShow;
        store as TraitStore;
    }

    public function __construct()
    {
        $this->middleware('auth.api');
    }

    public function download(File $file, string $filename) {

        // If accessing by a different name than the file is known by,
        // redirect to the correct URL for SEO reasons
        if ($file->clientName != $filename) {
            return redirect()->to(route('api.download', [$file, $file->clientName]));
        }

        if (
            $file->visibility == "private" &&
            (
                \Auth::guest() ||
                \Auth::user()->id != $file->user->id
            )
        ) {
            abort(403, 'You are not authorized to view this file.');
        }

        $file->downloads++;

        $file->save();

        return response()->download(storage_path('app/files/'.$file->internalName),$file->clientName);

    }

    public function index() {
        return response()->json(
            $this->TraitIndex()->makeHidden(['size','deleted_at','internalName'])
        );
    }

    public function show($id)
    {
        return response()->json(
            $this->TraitShow($id)->makeHidden(['user','deleted_at','size','internalName'])
        );
    }

    public function store(Request $request)
    {
        return response()->json(
            $this->TraitStore($request)->makeHidden(['size','deleted_at','internalName','user'])
        );
    }

}