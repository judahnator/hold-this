<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('faq', function() {
    return view('faq');
});

Route::post('files/upload','FileController@upload');
Route::get('files/{id}/{filename}','FileController@download')->name('download');
Route::resource('files','FileController',['only'=>['destroy','show','store','update']]);

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('tokens','TokenController',['only'=>['index','store','destroy']]);