<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth.api')->get('/user', function (Request $request) {
    return $request->user()->makeHidden(['files']);
});

Route::get('/status',function() {
    return response([
        'status' => 'ok'
    ]);
});

Route::group(['namespace'=>'API'],function() {

    Route::resource('files','FileController',['only'=>['show','store','index']]);
    Route::get('files/{file}/{filename}','FileController@download')->name('api.download');

});